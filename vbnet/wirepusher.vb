Imports System.Security.Cryptography
Imports System.Text
Imports RestSharp

'*** Credits for creating this library to: Daniel Castillo Macias - AdnDigital

Module Codificacion
    Public Function WPsend(id As String, title As String, message As String, type As String, Encryption_Password As String)
        Try
            If Trim(id) = "" Then
                Return False
                Exit Function
            End If
            Dim client As New RestClient("https://wirepusher.com")
            Dim request As New RestRequest("/send", Method.Get)
            request.AddQueryParameter("id", id)
            request.AddQueryParameter("type", type)
            If Encryption_Password <> "" Then

                Dim iv As Byte() = New Byte(15) {}
                Dim csp As New RNGCryptoServiceProvider()
                csp.GetBytes(iv)
                request.AddQueryParameter("iv", Bin2hex(iv))

                Dim HashedPassword As String = PasswordEncryptSHA(Encryption_Password)

                Dim llave As Byte() = New Byte(15) {}
                llave = Hex2bin(HashedPassword)

                request.AddQueryParameter("title",
                WirePusherEncryptData(title, iv, llave))

                request.AddQueryParameter("message",
                WirePusherEncryptData(message, iv, llave))

            Else
                request.AddQueryParameter("title", title)
                request.AddQueryParameter("message", message)
            End If
            Dim response = client.ExecuteAsync(request)
            Return response
        Catch ex As Exception
            MsgBox(Err.Description)
            Return False
        End Try
    End Function

    Public Function WirePusherEncryptData(plaintext As String, iv As Byte(), key As Byte()) As String
#Disable Warning SYSLIB0021 ' El tipo o el miembro están obsoletos
        Dim cryptor As New AesManaged With {.Mode = CipherMode.CBC, .Padding = PaddingMode.PKCS7, .KeySize = 128, .BlockSize = 128}
#Enable Warning SYSLIB0021 ' El tipo o el miembro están obsoletos
        Dim plaintextBytes() As Byte = System.Text.Encoding.Unicode.GetBytes(plaintext)
        Dim ms As New System.IO.MemoryStream
        Dim encStream As New CryptoStream(ms, cryptor.CreateEncryptor(key, iv), CryptoStreamMode.Write)
        encStream.Write(plaintextBytes, 0, plaintextBytes.Length)
        encStream.FlushFinalBlock()
        Return Convert.ToBase64String(ms.ToArray).Replace("+", "-").Replace("/", ".").Replace("=", "_")
    End Function

    Public Function Bin2hex(bytes As Byte())
        Dim sb = New StringBuilder()
        For Each b In bytes
            sb.Append(b.ToString("x2"))
        Next
        Return sb.ToString().ToString.Substring(0, 32)
    End Function

    Public Function Hex2bin(ByVal Hex As String) As Byte()
        Dim B As Byte() = Enumerable.Range(0, Hex.Length).Where(Function(x) x Mod 2 = 0).[Select](Function(x) Convert.ToByte(Hex.Substring(x, 2), 16)).ToArray()
        Return B
    End Function

    Public Function PasswordEncryptSHA(ByVal password As String)
        Dim sha As SHA1 = SHA1.Create
        Dim bytesToHash() As Byte
        bytesToHash = System.Text.Encoding.ASCII.GetBytes(password)
        bytesToHash = sha.ComputeHash(bytesToHash)
        Dim encPassworda As String = ""
        Dim EncPassword As String
        For Each b As Byte In bytesToHash
            encPassworda += b.ToString("x2")
        Next
        EncPassword = encPassworda.ToString.Substring(0, 32)
        Return EncPassword
    End Function

End Module