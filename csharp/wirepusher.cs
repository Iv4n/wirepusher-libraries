using RestSharp;
using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

/* Credits for creating this library to: Paludour - paludour.net */
namespace Wirepusher
{
	public static class Wirepusher
	{
		public static void Send(string id, string title, string message, string type = null, string encryption_password = null)
		{
			RestClient client = new RestClient("https://wirepusher.com");
			RestRequest request = new RestRequest("/send", Method.GET);
			request.AddQueryParameter("id", id);
			if (!string.IsNullOrEmpty(type))
				request.AddQueryParameter("type", type);
			if (!string.IsNullOrEmpty(encryption_password))
			{
				byte[] iv = new byte[16];
				using (RNGCryptoServiceProvider csp = new RNGCryptoServiceProvider())
					csp.GetBytes(iv);
				request.AddQueryParameter("iv", bin2hex(iv));
				string hashed_password;
				using (SHA1 sha1 = SHA1.Create())
					hashed_password = bin2hex(sha1.ComputeHash(Encoding.UTF8.GetBytes(encryption_password))).Substring(0, 32);
				request.AddQueryParameter("title", wirePusher_encrypt(title, iv, hex2bin(hashed_password)));
				request.AddQueryParameter("message", wirePusher_encrypt(message, iv, hex2bin(hashed_password)));
			}
			else
			{
				request.AddQueryParameter("title", title);
				request.AddQueryParameter("message", message);
			}
			IRestResponse response = client.Get(request);
		}

		private static string bin2hex(byte[] bytes)
		{
			StringBuilder sb = new StringBuilder();
			foreach (byte b in bytes)
				sb.Append(b.ToString("x2"));
			return sb.ToString();
		}

		private static byte[] hex2bin(string hex)
		{
			return Enumerable.Range(0, hex.Length)
											 .Where(x => x % 2 == 0)
											 .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
											 .ToArray();
		}

		private static string wirePusher_encrypt(string text, byte[] iv, byte[] key)
		{
			using (AesManaged cryptor = new AesManaged { Mode = CipherMode.CBC, Padding = PaddingMode.PKCS7, KeySize = 128, BlockSize = 128 })
			{
				using (MemoryStream ms = new MemoryStream())
				{
					using (CryptoStream cs = new CryptoStream(ms, cryptor.CreateEncryptor(key, iv), CryptoStreamMode.Write))
					{
						using (StreamWriter swEncrypt = new StreamWriter(cs))
						{
							swEncrypt.Write(text);
						}
					}
					return Convert.ToBase64String(ms.ToArray()).Replace('+', '-').Replace('/', '.').Replace('=', '_');
				}
			}
		}
	}
}
